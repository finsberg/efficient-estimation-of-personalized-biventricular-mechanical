import os
import warnings


import seaborn as sns
import matplotlib as mpl
# mpl.use("Agg")
import matplotlib.pyplot as plt
import numpy as np


passive_filling_duration_ = {'MR011': 2,
                             'MR015': 2,
                             'CRT27': 2,
                             'MR016': 2,
                             'CRT17': 3,
                             'MR008': 2,
                             'CRT07': 10,
                             'MR019': 2}
p2key ={"CRT07":"CASE1",
        "CRT17":"CASE2",
        "CRT27": "CASE3",
        "MR011": "PH1",
        "MR016": "PH2"}

curdir = os.path.dirname(os.path.abspath(__file__))
es_points_ = {"CRT07":21,
              "CRT17": 14,
              "CRT27": 14}
avo = {"CRT07":12,
       "CRT17": 5,
       "CRT27": 4}
avo_int ={"CRT07":175,
          "CRT17": 160,
          "CRT27": 155}

avc = {"CRT07":24,
       "CRT17": 18,
       "CRT27": 17}
avc_int ={"CRT07":435,
          "CRT17": 395,
          "CRT27": 430}



angles_ = [30,40,50,60,70,80,90]
angles = [30,40,50,60,70,80]
patients = ["CRT07", "CRT17", "CRT27"]
ress = ["low", "high"]
As_  = [float(2**i) for i in range(-3,3)]
As  = [float(2**i) for i in range(-1,3)]

  
fname = "/".join([curdir, "/results/all_results.npy"])
results = np.load(fname).item()

fname = "/".join([curdir, "/results/all_results_active_stress.npy"])
results_active_stress = np.load(fname).item()


fname = "/".join([curdir, "/results/all_results_startsens.npy"])
results_startsens = np.load(fname).item()

fname = "/".join([curdir, "/results/all_results_mesh_conv.npy"])
results_mesh_conv = np.load(fname).item()


# Mesh volumes
meshvols = {}
for p in patients:
    for a in angles:
        if a == 60:
            for r in ress:
                key = "{}:{}:{}".format(p,r,a)
                meshvols[key] = results_mesh_conv[key]["meshvols"]
        else:
            key = "{}:low:{}".format(p,a)
            meshvols[key] = results[key]["meshvols"]
            
outdir = "/".join(["figures"])
if not os.path.exists(outdir):
    os.makedirs(outdir)

def get_colormap(ncolors, transparent = False):

    if transparent:
        
        if ncolors == 1:
            return "gray"
        elif ncolors == 2:
            return [sns.xkcd_rgb["pale red"],
                    sns.xkcd_rgb["denim blue"]]
            # return [sns.xkcd_rgb["light blue"],
            #     sns.xkcd_rgb["pale pink"]]
        elif ncolors == 3:
            return [sns.xkcd_rgb["pale red"],
                    sns.xkcd_rgb["denim blue"],
                    sns.xkcd_rgb["medium green"]]

    if ncolors == 1:
        return "k"
    elif ncolors == 2:
        return [sns.xkcd_rgb["pale red"],
                sns.xkcd_rgb["denim blue"]]
    elif ncolors == 3:
        return [sns.xkcd_rgb["pale red"],
                sns.xkcd_rgb["denim blue"],
                sns.xkcd_rgb["medium green"]]
    elif ncolors == 4:
        return [sns.xkcd_rgb["pale red"],
                sns.xkcd_rgb["denim blue"],
                sns.xkcd_rgb["medium green"],
                sns.xkcd_rgb["amber"]]
    else:
        return sns.color_palette("Paired", ncolors)

def tabalize(caption, header, table, label, floatfmt=".2e"):

    try:
        import tabulate
        tabulate.LATEX_ESCAPE_RULES = {}
    except:
        has_tabulate=False
    else:
        has_tabulate=True
    
    if not has_tabulate:
        print("Please install tabulate. pip install tabulate")
        return table

    tabular =  tabulate.tabulate(table, header,
                                 tablefmt="latex", floatfmt=floatfmt)
    T = \
        r"""
\begin{{table}}
\caption{{{}}}
{}
\label{{{}}}
\end{{table}}
""".format(*[caption, tabular, label])
        
    return T


def get_Istrain(data, split=False):

    n = np.count_nonzero(data["features_scalar"]["gamma"]["0"])
    I_strains = [ np.subtract(data["measured_strain"]["circumferential"][k][-n:],
                                      data["simulated_strain"]["circumferential"][k][-n:])**2 \
                          for k in range(3)]

    if split:
        return np.mean(I_strains, 1)
    else:
        return np.mean(I_strains) 

def get_Ivol(data, split=False):
    n = np.count_nonzero(data["features_scalar"]["gamma"]["0"])
    I_vols = [np.divide(np.subtract(data["simulated_volume{}".format(k)][-n:],
                                   data["measured_volume{}".format(k)][-n:]),
                       data["measured_volume{}".format(k)][-n:])**2 for k in ["", "_rv"]]

    
    if split:
        return np.mean(I_vols,1)
    else:
        return np.mean(I_vols)


def interpolate(arr, p, return_original=False):

    import scipy
    # Make 100 point from start to end-diastole
    # i.e passive filling
    ed = passive_filling_duration_[p]+1

    passive_filling = arr[:ed]
    x_passive_filling = np.linspace(0,1, ed)
    

    x_passive_filling_int = np.linspace(0,1, 100)
    passive_filling_int = np.interp(x_passive_filling_int,
                                    x_passive_filling,
                                    passive_filling)


    es = es_points_[p]+1
    
    systole = arr[ed-1:es]
    x_systole = np.linspace(0,1,len(systole))

    x_systole_int = np.linspace(0,1, 200)
    systole_int = np.interp(x_systole_int,
                            x_systole,systole)

    
    diastole = arr[es:]
    x_diastole = np.linspace(0,1,len(diastole))

    x_diastole_int = np.linspace(0,1, 300)
    diastole_int = np.interp(x_diastole_int,
                            x_diastole,diastole)

    
    new_arr = np.concatenate((passive_filling_int,
                              systole_int,
                              diastole_int))


    ticks = (100, 300)
    tickslabels = ("ED", "ES")

    return new_arr, ticks, tickslabels



def cm2inch(*tupl):
    inch = 2.54
    if isinstance(tupl[0], tuple):
        return tuple(i/inch for i in tupl[0])
    else:
        return tuple(i/inch for i in tupl)

    
def setup_plot(fontsize=7,dpi=300,height=9.5, width=9.5, autolayout=True, save_format="png"):
    
    # Plotting options

    sns.set_palette("husl")
    sns.set_style("white")
    sns.set_style("ticks")
    mpl.rcParams.update({'figure.autolayout': autolayout})

  
    
    mpl.rcParams['font.family']= 'times'
    mpl.rcParams['font.weight']= 'normal'
    mpl.rcParams['font.size']= fontsize
    mpl.rcParams['font.size']= fontsize

    # width in cm

    # minimal size
    # width = 3
    # # single column
    # width = 9
    # # 1.5 column
    # width = 14
    # # full width
    # width = 19

    # # For two images side by side, leave some space
    # width = 9.5
    # height = 9.5
    
    # width = 19.5
    # height = 19.5
    inch = cm2inch(width, height)
    print inch
    
    mpl.rcParams['figure.figsize']= inch #(2.5,2.5)
    
    

    mpl.rcParams['xtick.labelsize'] = fontsize
    mpl.rcParams['ytick.labelsize'] = fontsize
    mpl.rcParams['legend.fontsize'] = fontsize
    mpl.rcParams['axes.titlesize'] = fontsize
    mpl.rcParams['axes.labelsize'] = fontsize

    # mpl.rcParams['figure.dpi'] = 30
    mpl.rcParams['savefig.dpi'] = dpi
    mpl.rcParams['savefig.format'] = save_format
    mpl.rcParams['text.usetex'] = True
    mpl.rcParams['text.latex.unicode']=True
    
    # Surpress warnings from matplotlib
    warnings.filterwarnings("ignore", module="matplotlib")
