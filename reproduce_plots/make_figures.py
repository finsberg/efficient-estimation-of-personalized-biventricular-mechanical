
from utils import *




def figure3():

    
    setup_plot(8, 1000, height=9, width=16, save_format="eps", autolayout=False)
    # sns.set_style("white")
    sns.set_style("whitegrid")
    mpl.rcParams['font.family']= 'times'
    mpl.rcParams['font.weight']= 'normal'
    mpl.rcParams['lines.linewidth']= 0.9

    import matplotlib.gridspec as gridspec
    fig = plt.figure()

    gs1=  gridspec.GridSpec(3,3)
    gs1.update(left=0.05, right=0.35, wspace=0.05)

    gs2 = gridspec.GridSpec(3,3)
    gs2.update(left=0.43, right=0.73, hspace=0.05)

    gs3 = gridspec.GridSpec(3,1)
    gs3.update(left=0.85, right=0.98, hspace=0.4)

    ax1 = []
    ax2  = []
    ax3 = []
    for i in range(3):
        ax1_ = []
        ax2_ = []
        ax3.append(plt.subplot(gs3[i:i+1]))
        fig.add_subplot(ax3[-1])
        for j in range(3):

            ax1_.append(plt.subplot(gs1[i:i+1, j:j+1]))
            ax2_.append(plt.subplot(gs2[i:i+1, j:j+1]))

            fig.add_subplot(ax1_[-1])
            fig.add_subplot(ax2_[-1])

        if i==2:
            for ax in ax1_: ax.set_visible(False)
            ax3[-1].set_visible(False)

        ax1.append(ax1_)
        ax2.append(ax2_)



    split =False
    r = "low"
    I_strain = {}
    I_vol = {}

    for p in patients:
        I_strain[p] = []
        I_vol[p] = []

        for a in angles_:

            key = "{}:{}:{}".format(p, r, a)
            if not results.has_key(key): continue

            I_strain[p].append(get_Istrain(results[key], split))
            I_vol[p].append(get_Ivol(results[key], split))

    direction = "circumferential"
    r = "low"

    colors = get_colormap(len(patients))
    markers = ["s", "o", "v"]

    for i, p in enumerate(patients):
        if I_vol[p]:
            ax3[0].plot(angles_, I_vol[p], label=p2key[p], color=colors[i], marker = markers[i])
    ax3[0].set_ylabel(r"$\overline{\mathcal{J}_{\mathrm{volume}}}$")
    ax3[0].yaxis.major.formatter.set_powerlimits((0,0)) 

    lines = []
    labels = []
    for i, p in enumerate(patients):
        if I_strain[p]:
            line = ax3[1].plot(angles_, I_strain[p], label=p2key[p], color=colors[i], marker = markers[i])
            lines.append(line[0])
            labels.append(p2key[p])


    ax3[0].set_xticks([30,50,70,90])
    ax3[0].set_xticklabels([])
    ax3[1].set_xticks([30,50,70,90])
    ax3[1].set_xlabel("Fiber angle")
    ax3[1].set_ylabel(r"$\overline{\mathcal{J}_{\mathrm{strain}}}$")
    ax3[1].yaxis.major.formatter.set_powerlimits((0,0))

    colors = sns.color_palette("husl", len(angles))  
    for j, p in enumerate(patients):

        name_ = "{}:{}:{}".format(p, r,angles[0])
        if not results.has_key(name_): continue
        lvv = results[name_]["measured_volume"][1:]
        lvp = results[name_]["pressure"]
        rvv = results[name_]["measured_volume_rv"][1:]
        rvp = results[name_]["rv_pressure"]


        gs = results[name_]["features_scalar"]["gamma"]["0"]
        npassive = len(gs)-np.count_nonzero(gs)
        strainyticks = [-0.3,-0.2, -0.1, 0.0]

        for i, a in enumerate(angles):


            name = "{}:{}:{}".format(p,r, a)
            if not results.has_key(name): continue

            lvv_sim = results[name]["simulated_volume"]

            ax1[0][j].plot(lvv_sim,lvp, color = colors[i], linestyle = "-",label = "{}".format(a))


            rvv_sim = results[name]["simulated_volume_rv"]

            ax1[1][j].plot(rvv_sim,rvp, color = colors[i], linestyle = "-",label = "{}".format(a))


        ax1[0][j].plot(lvv,lvp[1:], markerfacecolor="None",
                markeredgecolor = "k", markeredgewidth = 0.5,
                linestyle = "", marker = "o", markersize=5, label = "Measured")

        ax1[0][j].set_xticklabels([])
        if j > 0:
            ax1[0][j].set_yticklabels([])
            ax1[1][j].set_yticklabels([])

        ax1[1][j].plot(rvv,rvp[1:], markerfacecolor="None",
                markeredgecolor = "k", markeredgewidth = 0.5,
                linestyle = "", marker = "o", markersize=5, label = "Measured")


        for l in range(2):
            ax1[l][j].set_xticks([25, 75,120])
            ax1[l][j].locator_params(axis="y", nbins=4)
            ax1[l][j].set_xlim([0,150])

        # ax1[2,j].set_visible(False)
        # continue

        if j == 2:

            for k, t in enumerate(["LV", "RV", "Septum"]):
                ax_  = ax2[k][j].twinx()
                ax_.yaxis.set_label_position("right")
                ax_.set_yticks([])
                ax_.set_ylabel(t)

            for k, t in enumerate(["LV", "RV"]):
                ax_  = ax1[k][j].twinx()
                ax_.yaxis.set_label_position("right")
                ax_.set_yticks([])
                ax_.set_ylabel(t)

        for k, a in enumerate(angles):

            name = "{}:{}:{}".format(p,r, a)
            if not results.has_key(name): continue

            s,_,_ = interpolate(results[name]["simulated_strain"][direction][0][1:],p)
            ax2[0][j].plot(s, color = colors[k], linestyle = "-")

            s,_,_ = interpolate(results[name]["simulated_strain"][direction][2][1:],p)
            ax2[1][j].plot(s, color = colors[k], linestyle = "-")

            s,_,_ = interpolate(results[name]["simulated_strain"][direction][1][1:],p)
            ax2[2][j].plot(s, color = colors[k], linestyle = "-")




        #LV
        stk = [0,2,1]
        for k  in range(3):
            m, ticks, ticklabels = interpolate(results[name]["measured_strain"][direction][stk[k]], p)
            x = range(len(m))
            x_ = x[::20]

            ax2[k][j].plot(x_,m[::20], markerfacecolor="None",
                           markeredgecolor = "k", markeredgewidth = 0.5,
                           linestyle = "", marker = "o", markersize=5, label = "Measured")
            ax2[k][j].set_xticks(ticks)
            if k < 2:
                ax2[k][j].set_xticklabels([])
            else:
                ax2[k][j].set_xticklabels(ticklabels)
            if j > 0 : ax2[k][j].set_yticklabels([])
            ax2[k][j].set_ylim([-0.3, 0.05])
            ax2[k][j].set_yticks(strainyticks)
            ax2[k][j].plot([avo_int[p],avo_int[p]], [-0.3, 0.05], "k:")
            ax2[k][j].plot([avc_int[p],avc_int[p]], [-0.3, 0.05], "k:")


        ax1[0][j].set_title(p2key[p])
        ax2[0][j].set_title(p2key[p])

    lines = ax3[0].lines
    labels = ["CASE1", "CASE2", "CASE3"]
    fig.legend(lines, labels, loc = (0.85,0.12))



    ax1[1][1].set_xlabel("Volume (ml)", labelpad=5)

    labels = ["${}$".format(ai) for ai in angles]
    lines = ax1[0][0].lines

    txt1=plt.figtext(0.2,0.97,"PV loops", va="center", ha="center", size=11)
    txt2=plt.figtext(0.58,0.97,"Circumferential strain", va="center", ha="center", size=11)
    txt3=plt.figtext(0.9,0.97,"Cost functionals", va="center", ha="center", size=11)
    txt4=plt.figtext(0.01,0.65,"Pressure (kPa)", va="center", ha="center",
                     rotation="vertical",size=8)

    leg = fig.legend(lines, labels, loc = (0.05, 0.1),
                      title="Fiber angle",
                      ncol=len(angles)/2)

    ext = (leg, txt1, txt2, txt3, txt4)

    fig.savefig(os.path.join(outdir, "figure3"), bbox_extra_artists=ext, bbox_inches='tight')

    plt.close()



def figure4():

    setup_plot(7, 300, height=9, width=9, save_format="eps", autolayout=False)
    # sns.set_style("white")
    sns.set_style("whitegrid")
    mpl.rcParams['font.family']= 'times'
    mpl.rcParams['font.weight']= 'normal'
    mpl.rcParams['lines.linewidth']= 0.9

    colors = sns.color_palette("husl", len(angles))

   
    r = "low"

    regions = [0,1,2]
    labels = ["LV", "Septum", "RV"]

    for direction in ["longitudinal"]:



        fig1, ax1 = plt.subplots(3,3, sharey=True, sharex=True)
        for j,p in enumerate(patients):

            name_ = "{}:{}:{}".format(p, r,angles[0])
            if not results.has_key(name_): continue


            ax1[0,j].set_title(p2key[p])
            if j == 2:

                for k,l in enumerate(["LV", "Septum", "RV"]):
                    ax1_  = ax1[k,j].twinx()
                    ax1_.yaxis.set_label_position("right")
                    ax1_.set_yticks([])
                    ax1_.set_ylabel(l)


            for i, (region, l) in enumerate(zip(regions, labels)):

                # m = results[name_]["measured_strain"][direction][region]
                m, ticks, ticklabels = interpolate(results[name_]["measured_strain"][direction][region], p)
                x = range(len(m))
                x_ = x[::20]

                for k, a in enumerate(angles):

                    name = "{}:{}:{}".format(p,r, a)
                    if not results.has_key(name): continue

                    # s = results[name]["simulated_strain"][direction][region][1:]
                    s,_,_ = interpolate(results[name]["simulated_strain"][direction][region][1:],p)


                    ax1[i,j].plot(s, color = colors[k], linestyle = "-",label = "{}".format(a))

                ax1[i,j].plot(x_, m[::20], markerfacecolor="None",
                        markeredgecolor = "k", markeredgewidth = 0.5,
                        linestyle = "", marker = "o", markersize=5, label = "Measured")


        lines = ax1[0,0].lines
        labels_ = ["${}$".format(ai) for ai in angles]

        for i, ax in enumerate(ax1.flatten().tolist()):
            # ax.set_title(l)
            ax.locator_params(axis="y", nbins=5)
            ax.locator_params(axis="x", nbins=3)

            ax.set_xticks(ticks)
            ax.set_xticklabels(ticklabels)
       


        legg = fig1.legend(lines, labels_,
                           loc="center right",   
                           borderaxespad=0.1,
                           title="Fiber angle")

        ax_=fig1.add_subplot(111, frameon=False)
        ax_.grid(False)
        ax_.set_xticks([])
        ax_.set_yticks([])
        ax_.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')
        ax_.set_ylabel("{} strain".format(direction.capitalize()), labelpad=30)
        ax_.set_xlabel("Time point", labelpad=20)

        fig1.subplots_adjust(right=0.8)
        fig1.savefig(os.path.join(outdir, "figure4"),
                     bbox_extra_artists=(legg,), bbox_inches='tight')

        plt.close()


def figure5():

    setup_plot(8, 500, height=9, width=16, save_format="eps", autolayout=False)
   
    sns.set_style("whitegrid")
    mpl.rcParams['font.family']= 'times'
    mpl.rcParams['font.weight']= 'normal'
    mpl.rcParams['lines.linewidth']= 0.9


    import matplotlib.gridspec as gridspec

    fig = plt.figure()

    gs1 = gridspec.GridSpec(2, 3)
    gs1.update(left=0.05, right=0.48, wspace=0.05)

    gs2 = gridspec.GridSpec(2, 3)
    gs2.update(left=0.55, right=0.98, hspace=0.05)

    ax1 = []
    ax2  = []
    for i in range(2):
        ax1_ = []
        ax2_ = []
        for j in range(3):

            ax1_.append(plt.subplot(gs1[i:i+1, j:j+1]))
            ax2_.append(plt.subplot(gs2[i:i+1, j:j+1]))

            fig.add_subplot(ax1_[-1])
            fig.add_subplot(ax2_[-1])

        ax1.append(ax1_)
        ax2.append(ax2_)
        
    colors = sns.color_palette("husl", len(angles))  

    for j, p in enumerate(patients):
        lines = []
        for i, a in enumerate(angles):

            name = "{}:{}:{}".format(p, "low", a)

            if j == 2:

                for k, t in enumerate(["LV", "RV"]):
                    ax3  = ax2[k][j].twinx()
                    ax3.yaxis.set_label_position("right")
                    ax3.set_yticks([])
                    ax3.set_ylabel(t)


            if i == 0:


                ax1[0][j].set_title(p2key[p])
                ax2[0][j].set_title(p2key[p])


            if not results.has_key(name): continue


            meshvol = meshvols[name]

            # Gamma
            g_lv = results[name]["features_scalar"]['gamma']["0"]
            g_septum = results[name]["features_scalar"]['gamma']["1"]


            g_lvsept = np.add(np.multiply(g_lv,meshvol[0]),
                              np.multiply(g_septum,meshvol[1]))/(meshvol[0]+meshvol[1])

            g_rv = results[name]["features_scalar"]['gamma']["2"]

            # Add final zero
            g_lvsept = np.append(g_lvsept, 0.0)
            g_rv = np.append(g_rv, 0.0)


            G_lv, ticks, ticklabels = interpolate(g_lvsept, p)
            G_rv, ticks, ticklabels = interpolate(g_rv, p)

            l, = ax1[0][j].plot(G_lv, color = colors[i])
            lines.append(l)
            ax1[1][j].plot(G_rv, color = colors[i])



            for k in range(2):
                ax1[k][j].plot([avo_int[p],avo_int[p]], [0,0.4], "k:")
                ax1[k][j].plot([avc_int[p],avc_int[p]], [0,0.4], "k:")
                ax1[k][j].set_xticks(ticks)
                ax1[k][j].set_yticks([0,0.1, 0.2, 0.3, 0.4])
                ax1[k][j].set_ylim([0, 0.4])
                if j>0: ax1[k][j].set_yticklabels([])


            ax1[0][j].set_xticklabels([])
            ax1[1][j].set_xticklabels(ticklabels)



            # Stress
            tf_lv = results[name]["features_scalar"]['cauchy_stress:fiber']["0"]
            tf_septum = results[name]["features_scalar"]['cauchy_stress:fiber']["1"]
            tf_lvsept = np.add(np.multiply(tf_lv,meshvol[0]),
                               np.multiply(tf_septum,meshvol[1]))/(meshvol[0]+meshvol[1])

            tf_rv = results[name]["features_scalar"]['cauchy_stress:fiber']["2"]

            Tf_lv, ticks, ticklabels = interpolate(tf_lvsept, p)
            Tf_rv, ticks, ticklabels = interpolate(tf_rv, p)

            ax2[0][j].plot(Tf_lv, color = colors[i])
            ax2[1][j].plot(Tf_rv, color = colors[i])

            ax2[0][j].set_xticklabels([])
            ax2[1][j].set_xticklabels(ticklabels)

            for k in range(2):
                ax2[k][j].plot([avo_int[p],avo_int[p]], [0,80], "k:")
                ax2[k][j].plot([avc_int[p],avc_int[p]], [0,80], "k:")
                ax2[k][j].set_xticks(ticks)
                ax2[k][j].set_yticks([0,20,40,60,80])
                ax2[k][j].set_ylim([0, 80])
                if j>0: ax2[k][j].set_yticklabels([])


    labels = ["${}$".format(ai) for ai in angles]
    leg = fig.legend(lines, labels, loc =(0.22, -0.01), ncol=len(angles))#, title="Fiber angle")
    txt1=plt.figtext(0.78,0.97,"Fiber stress (kPa)", va="center", ha="center", size=11)
    txt2=plt.figtext(0.28,0.97,"Active strain", va="center", ha="center", size=11)
    fig.savefig(os.path.join(outdir, "figure5"), bbox_extra_artists=(leg,txt1,txt2), bbox_inches='tight')





def figure6():
    
 
    setup_plot(8, 500, height=6, width=14, save_format="eps", autolayout=False)
    
    sns.set_style("whitegrid")
    mpl.rcParams['font.family']= 'times'
    mpl.rcParams['font.weight']= 'normal'
    mpl.rcParams['lines.linewidth']= 0.9


    colors = get_colormap(2)

    
    import matplotlib.gridspec as gridspec

    fig = plt.figure()

    gs1 = gridspec.GridSpec(2, 3)
    gs1.update(left=0.02, right=0.37, wspace=0.05)

    gs2 = gridspec.GridSpec(2, 3)
    gs2.update(left=0.45, right=0.84, hspace=0.15)

    ax1 = []
    ax2  = []
    for i in range(2):
        ax1_ = []
        ax2_ = []
        for j in range(3):

            ax1_.append(plt.subplot(gs1[i:i+1, j:j+1]))
            ax2_.append(plt.subplot(gs2[i:i+1, j:j+1]))

            fig.add_subplot(ax1_[-1])
            fig.add_subplot(ax2_[-1])

        ax1.append(ax1_)
        ax2.append(ax2_)


    get_lines = True
    a = 60
    for j, p in enumerate(patients):
        for i, a in enumerate(["active_stress", "active_strain"]):

            name = "{}:{}".format(p, a)

            print name
            

            meshvol = meshvols["{}:low:60".format(p)]


            # Stress
            tf_lv = results_active_stress[name]["features_scalar"]['cauchy_stress:fiber']["0"]
            tf_septum = results_active_stress[name]["features_scalar"]['cauchy_stress:fiber']["1"]

            tf_lvsept = np.add(np.multiply(tf_lv,meshvol[0]),
                               np.multiply(tf_septum,meshvol[1]))/(meshvol[0]+meshvol[1])

            tf_rv = results_active_stress[name]["features_scalar"]['cauchy_stress:fiber']["2"]


            Tf_lv, ticks, ticklabels = interpolate(tf_lvsept, p)
            Tf_rv, ticks, ticklabels = interpolate(tf_rv, p)



            # Gamma
            g_lv = results_active_stress[name]["features_scalar"]['gamma']["0"]
            g_septum = results_active_stress[name]["features_scalar"]['gamma']["1"]


            g_lvsept = np.add(np.multiply(g_lv,meshvol[0]),
                               np.multiply(g_septum,meshvol[1]))/(meshvol[0]+meshvol[1])

            g_rv = results_active_stress[name]["features_scalar"]['gamma']["2"]

            # Add final zero
            g_lvsept = np.append(g_lvsept, 0.0)
            g_rv = np.append(g_rv, 0.0)

            G_lv, ticks, ticklabels = interpolate(g_lvsept, p)
            G_rv, ticks, ticklabels = interpolate(g_rv, p)


            if a == "active_stress":
                ax1[i][j].semilogy(G_lv, color = colors[0], linestyle = "-", label = "LV")
                ax1[i][j].semilogy(G_rv, color = colors[1], linestyle = "-", label = "RV")

                ax2[i][j].plot(Tf_lv, color = colors[0], linestyle = "-", label = "LV")
                ax2[i][j].plot(Tf_rv, color = colors[1], linestyle = "-", label = "RV")


            else:
                line1 = ax1[i][j].plot(G_lv, color = colors[0], linestyle = "-", label = "LV")
                line2 = ax1[i][j].plot(G_rv, color = colors[1], linestyle = "-", label = "RV")

                line1_ = ax2[i][j].plot(Tf_lv, color = colors[0], linestyle = "-", label = "LV")
                line2_ = ax2[i][j].plot(Tf_rv, color = colors[1], linestyle = "-", label = "RV")


                if get_lines:
                    lines = [line1[0], line2[0]]
                    labels = ["LV", "RV"]

                    get_lines =False


            if i == 0:
                ax1[i][j].set_title(p2key[p])
                ax2[i][j].set_title(p2key[p])

            ax1[i][j].set_xticks(ticks)
            ax1[i][j].set_xticklabels([])
            ax2[i][j].set_xticks(ticks)
            ax2[i][j].set_xticklabels([])


            if j == 0:
                if a == "active_stress":
                    ax1[i][j].set_ylabel("Active stress")
                else:
                    ax1[i][j].set_ylabel("Active strain")




            if a == "active_stress":
                # axij.set_yticks([0,100, 200, 300, 400, 500])
                ax1[i][j].set_ylim([1, 500])
                ax1[i][j].plot([avo_int[p],avo_int[p]], [1,500], "k:")
                ax1[i][j].plot([avc_int[p],avc_int[p]], [1,500], "k:")

            else:
                ax1[i][j].set_yticks([0,0.1, 0.2, 0.3, 0.4])
                ax1[i][j].set_ylim([0, 0.4])
                ax1[i][j].plot([avo_int[p],avo_int[p]], [0,0.4], "k:")
                ax1[i][j].plot([avc_int[p],avc_int[p]], [0,0.4], "k:")

            ax2[i][j].set_ylim([0, 80])
            ax2[i][j].set_yticks([0,20,40,60,80])
            ax2[i][j].plot([avo_int[p],avo_int[p]], [0,80], "k:")
            ax2[i][j].plot([avc_int[p],avc_int[p]], [0,80], "k:")

            if j > 0:
                ax1[i][j].set_yticklabels([])
                ax2[i][j].set_yticklabels([])

        ax1[1][j].set_xticklabels(ticklabels)
        ax2[1][j].set_xticklabels(ticklabels)



        lgd = fig.legend(lines, labels,
                      loc=(0.88, 0.4), 
                      borderaxespad=0.1)
    fig.subplots_adjust(right=0.8)

    txt1=plt.figtext(0.21,1.0,"Cardiac contraction", va="center", ha="center", size=11)
    txt2=plt.figtext(0.64,1.0,"Fiber stress (kPa)", va="center", ha="center", size=11)

    fig.savefig(os.path.join(outdir, "figure6"), bbox_extra_artists=(lgd,txt1,txt2), bbox_inches='tight')


    plt.close("all")

def figure7():

    
    ress = ["low", "high"]
    maindir = "results_active_strain1"

 
    setup_plot(8, 500, height=6, width=14, save_format="eps", autolayout=False)
    # sns.set_style("white")
    sns.set_style("whitegrid")
    mpl.rcParams['font.family']= 'times'
    mpl.rcParams['font.weight']= 'normal'
    mpl.rcParams['lines.linewidth']= 0.9
    colors = get_colormap(2)
 
    import matplotlib.gridspec as gridspec
 
    fig = plt.figure()
         
    gs1 = gridspec.GridSpec(2, 3)
    gs1.update(left=0.02, right=0.37, wspace=0.05)
     
    gs2 = gridspec.GridSpec(2, 3)
    gs2.update(left=0.45, right=0.84, hspace=0.15)
         
    ax1 = []
    ax2 = []
    for i in range(2):
        ax1_ = []
        ax2_ = []
        for j in range(3):
 
            ax1_.append(plt.subplot(gs1[i:i+1, j:j+1]))
            ax2_.append(plt.subplot(gs2[i:i+1, j:j+1]))
 
            fig.add_subplot(ax1_[-1])
            fig.add_subplot(ax2_[-1])
 
        ax1.append(ax1_)
        ax2.append(ax2_)
 
 
    res2label = {"low": "coarse", "high":"refined"}
    get_lines = True
    lines = []
    labels=[]
    a = 60
    linestyles = ["-", "--"]
    for j, p in enumerate(patients):
        for i, r in enumerate(["low", "high"]):
 
            name = "{}:{}:{}".format(p, r, a)
 
            meshvol = meshvols[name]
 
 
            # Stress
            tf_lv = results_mesh_conv[name]["features_scalar"]['cauchy_stress:fiber']["0"]
            tf_septum = results_mesh_conv[name]["features_scalar"]['cauchy_stress:fiber']["1"]
 
            tf_lvsept = np.add(np.multiply(tf_lv,meshvol[0]),
                               np.multiply(tf_septum,meshvol[1]))/(meshvol[0]+meshvol[1])
 
            tf_rv = results_mesh_conv[name]["features_scalar"]['cauchy_stress:fiber']["2"]
 
 
            Tf_lv, ticks, ticklabels = interpolate(tf_lvsept, p)
            Tf_rv, ticks, ticklabels = interpolate(tf_rv, p)
 
 
 
            # Gamma
            g_lv = results_mesh_conv[name]["features_scalar"]['gamma']["0"]
            g_septum = results_mesh_conv[name]["features_scalar"]['gamma']["1"]
 
 
            g_lvsept = np.add(np.multiply(g_lv,meshvol[0]),
                               np.multiply(g_septum,meshvol[1]))/(meshvol[0]+meshvol[1])
 
            g_rv = results_mesh_conv[name]["features_scalar"]['gamma']["2"]
 
            # Add final zero
            g_lvsept = np.append(g_lvsept, 0.0)
            g_rv = np.append(g_rv, 0.0)
 
            G_lv, ticks, ticklabels = interpolate(g_lvsept, p)
            G_rv, ticks, ticklabels = interpolate(g_rv, p)
 
             
            # Low LV
            line, = ax1[0][j].plot(G_lv, color = colors[i], linestyle = linestyles[i])
            ax1[1][j].plot(G_rv, color = colors[i], linestyle = linestyles[i])
 
            ax2[0][j].plot(Tf_lv, color = colors[i], linestyle = linestyles[i])
            ax2[1][j].plot(Tf_rv, color = colors[i], linestyle = linestyles[i])
 
 
 
 
            if get_lines:
                lines.append(line)
                labels.append(res2label[r])
                if i == 1:
                    get_lines =False
 
 
            if i == 0:
                ax1[i][j].set_title(p2key[p])
                ax2[i][j].set_title(p2key[p])
 
            ax1[i][j].set_xticks(ticks)
            ax1[i][j].set_xticklabels([])
            ax2[i][j].set_xticks(ticks)
            ax2[i][j].set_xticklabels([])
 
            if j > 0:
                ax1[i][j].set_yticklabels([])
                ax2[i][j].set_yticklabels([])
 
            ax1[1][j].set_xticklabels(ticklabels)
            ax2[1][j].set_xticklabels(ticklabels)
 
            ax1[i][j].set_yticks([0,0.1, 0.2, 0.3, 0.4])
            ax1[i][j].set_ylim([0, 0.4])
            ax1[i][j].plot([avo_int[p],avo_int[p]], [0,0.4], "k:")
            ax1[i][j].plot([avc_int[p],avc_int[p]], [0,0.4], "k:")
 
            ax2[i][j].set_ylim([0, 80])
            ax2[i][j].set_yticks([0,20,40,60,80])
            ax2[i][j].plot([avo_int[p],avo_int[p]], [0,80], "k:")
            ax2[i][j].plot([avc_int[p],avc_int[p]], [0,80], "k:")
 
 
 
    lgd = fig.legend(lines, labels,
                     loc=(0.88, 0.4), 
                     borderaxespad=0.1)
 
     
 
    ax3  = ax2[0][-1].twinx()
    ax3.yaxis.set_label_position("right")
    ax3.set_yticks([])
    ax3.set_ylabel("LV")
    ax3  = ax2[1][-1].twinx()
    ax3.yaxis.set_label_position("right")
    ax3.set_yticks([])
    ax3.set_ylabel("RV")
 
 
    fig.subplots_adjust(right=0.8)
 
    txt1=plt.figtext(0.21,1.0,"Cardiac contraction", va="center", ha="center", size=11)
    txt2=plt.figtext(0.64,1.0,"Fiber stress (kPa)", va="center", ha="center", size=11)
 
    fig.savefig(os.path.join(outdir, "figure7"), bbox_extra_artists=(lgd,txt1,txt2), bbox_inches='tight')
 
 
    plt.close("all")
             
    

def figureA1():

  

    setup_plot(7, 500, height=8, width=16, save_format="eps", autolayout=False)
    
    sns.set_style("whitegrid")
    mpl.rcParams['font.family']= 'times'
    mpl.rcParams['font.weight']= 'normal'
    mpl.rcParams['lines.linewidth']= 0.9


    import matplotlib.gridspec as gridspec

    fig = plt.figure()

    gs0 = gridspec.GridSpec(2, 3)
    gs0.update(left=0.05, right=0.28, hspace=0.05)


    gs1 = gridspec.GridSpec(2, 3)
    gs1.update(left=0.35, right=0.63, wspace=0.05)

    gs2 = gridspec.GridSpec(2, 3)
    gs2.update(left=0.7, right=0.98, hspace=0.05)

    ax0 =[]
    ax1 = []
    ax2  = []
    for i in range(2):
        ax0_ = []
        ax1_ = []
        ax2_ = []
        for j in range(3):

            ax0_.append(plt.subplot(gs0[i:i+1, j:j+1]))
            ax1_.append(plt.subplot(gs1[i:i+1, j:j+1]))
            ax2_.append(plt.subplot(gs2[i:i+1, j:j+1]))

            fig.add_subplot(ax0_[-1])
            fig.add_subplot(ax1_[-1])
            fig.add_subplot(ax2_[-1])

        ax0.append(ax0_)
        ax1.append(ax1_)
        ax2.append(ax2_)
        
    colors = sns.color_palette("husl", len(As))  
    r = "low"
    for j, p in enumerate(patients):
        lines = []
        labels = []

        name_ = "{}:{}".format(p, As[0])
        lvv = results_startsens[name_]["measured_volume"][1:]
        lvp = results_startsens[name_]["pressure"]
        rvv = results_startsens[name_]["measured_volume_rv"][1:]
        rvp = results_startsens[name_]["rv_pressure"]
        gs = results_startsens[name_]["features_scalar"]["gamma"]["0"]
        npassive = len(gs)-np.count_nonzero(gs)



        for i, a in enumerate(As):

            name = "{}:{}".format(p, a)


            lvv_sim = results_startsens[name]["simulated_volume"]

            ax0[0][j].plot(lvv_sim[:npassive],lvp[:npassive], color = colors[i], linestyle = "-",
                        label = "{}".format(a))

            rvv_sim = results_startsens[name]["simulated_volume_rv"]

            ax0[1][j].plot(rvv_sim[:npassive],rvp[:npassive], color = colors[i], linestyle = "-",
                        label = "{}".format(a))


            ax0[0][j].plot(lvv[:npassive-1],lvp[1:npassive], markerfacecolor="None",
                          markeredgecolor = "k", markeredgewidth = 0.5,
                          linestyle = "", marker = "o", markersize=5, label = "Measured")

            ax0[1][j].plot(rvv[:npassive-1],rvp[1:npassive], markerfacecolor="None",
                          markeredgecolor = "k", markeredgewidth = 0.5,
                          linestyle = "", marker = "s", markersize=5, label = "Measured")


            ax0[0][j].set_xlim([0,150])
            ax0[1][j].set_xlim([0,150])
            ax0[0][j].set_xticks([25,120])
            ax0[1][j].set_xticks([25,120])
            ax0[0][j].set_xticklabels([])
            if j > 0:
                ax0[0][j].set_yticklabels([])
                ax0[1][j].set_yticklabels([])

            if j == 2:

                for k, t in enumerate(["LV", "RV"]):
                    ax3  = ax2[k][j].twinx()
                    ax3.yaxis.set_label_position("right")
                    ax3.set_yticks([])
                    ax3.set_ylabel(t)


            if i == 0:

                ax0[0][j].set_title(p2key[p])
                ax1[0][j].set_title(p2key[p])
                ax2[0][j].set_title(p2key[p])


          
            name = "{}:{}".format(p, a)

            print name
            if not results_startsens.has_key(name): continue



            meshvol = meshvols["{}:low:60".format(p)]

            # Gamma
            g_lv = results_startsens[name]["features_scalar"]['gamma']["0"]
            g_septum = results_startsens[name]["features_scalar"]['gamma']["1"]


            g_lvsept = np.add(np.multiply(g_lv,meshvol[0]),
                              np.multiply(g_septum,meshvol[1]))/(meshvol[0]+meshvol[1])

            g_rv = results_startsens[name]["features_scalar"]['gamma']["2"]

            # Add final zero
            g_lvsept = np.append(g_lvsept, 0.0)
            g_rv = np.append(g_rv, 0.0)


            G_lv, ticks, ticklabels = interpolate(g_lvsept, p)
            G_rv, ticks, ticklabels = interpolate(g_rv, p)


            if r == "low":
                labels.append("${}$".format(a))
                linestyle="-"
            else:
                labels.append("${}$ (refined)".format(a))
                linestyle = "--"

            l, = ax1[0][j].plot(G_lv, color = colors[i], linestyle=linestyle)
            ax1[1][j].plot(G_rv, color = colors[i], linestyle=linestyle)
            lines.append(l)



            for k in range(2):
                ax1[k][j].plot([avo_int[p],avo_int[p]], [0,0.4], "k:")
                ax1[k][j].plot([avc_int[p],avc_int[p]], [0,0.4], "k:")
                ax1[k][j].set_xticks(ticks)
                ax1[k][j].set_yticks([0,0.1, 0.2, 0.3, 0.4])
                ax1[k][j].set_ylim([0, 0.4])
                if j>0: ax1[k][j].set_yticklabels([])


            ax1[0][j].set_xticklabels([])
            ax1[1][j].set_xticklabels(ticklabels)



            # Stress
            tf_lv = results_startsens[name]["features_scalar"]['cauchy_stress:fiber']["0"]
            tf_septum = results_startsens[name]["features_scalar"]['cauchy_stress:fiber']["1"]
            tf_lvsept = np.add(np.multiply(tf_lv,meshvol[0]),
                               np.multiply(tf_septum,meshvol[1]))/(meshvol[0]+meshvol[1])

            tf_rv = results_startsens[name]["features_scalar"]['cauchy_stress:fiber']["2"]

            Tf_lv, ticks, ticklabels = interpolate(tf_lvsept, p)
            Tf_rv, ticks, ticklabels = interpolate(tf_rv, p)

            ax2[0][j].plot(Tf_lv, color = colors[i])
            ax2[1][j].plot(Tf_rv, color = colors[i])

            ax2[0][j].set_xticklabels([])
            ax2[1][j].set_xticklabels(ticklabels)

            for k in range(2):
                ax2[k][j].plot([avo_int[p],avo_int[p]], [0,80], "k:")
                ax2[k][j].plot([avc_int[p],avc_int[p]], [0,80], "k:")
                ax2[k][j].set_xticks(ticks)
                ax2[k][j].set_yticks([0,20,40,60,80])
                ax2[k][j].set_ylim([0, 80])
                if j>0: ax2[k][j].set_yticklabels([])



    leg = fig.legend(lines, labels, loc =(0.3, -0.01), ncol=4)
    txt0=plt.figtext(0.17,0.97,"Passive filling", va="center", ha="center", size=9)
    txt1=plt.figtext(0.85,0.97,"Fiber stress (kPa)", va="center", ha="center", size=9)
    txt2=plt.figtext(0.49,0.97,"Active strain", va="center", ha="center", size=9)
    fig.savefig(os.path.join(outdir, "figureA1"), bbox_extra_artists=(leg,txt1,txt2), bbox_inches='tight')
 
    

def table2():

    r = "low"
    a = 60
    comp = "fiber"
    
    lst = {}
      
    keylst = ["lv_ed_{}".format(comp),
              "rv_ed_{}".format(comp),
              "lv_es_{}".format(comp),
              "rv_es_{}".format(comp)]
    
    for k in keylst: lst[k] = [[] for i in patients]

    for i, p in enumerate(patients):

                  
        name = "{}:{}:{}".format(p, r, a)

        if not results.has_key(name): continue

        tf_lv = results[name]["features_scalar"]['cauchy_stress:{}'.format(comp)]["0"]
        tf_septum = results[name]["features_scalar"]['cauchy_stress:{}'.format( comp)]["1"]
        meshvol = meshvols[name]

        tf_lvsept = np.add(np.multiply(tf_lv,meshvol[0]),
                           np.multiply(tf_septum,meshvol[1]))/(meshvol[0]+meshvol[1])

        tf_rv = results[name]["features_scalar"]['cauchy_stress:{}'.format(comp)]["2"]

        tp = np.arange(len(tf_lv))

        lv, ticks, ticklabels = interpolate(tf_lvsept, p)
        rv, ticks, ticklabels = interpolate(tf_rv, p)

        lst["lv_ed_{}".format(comp)][i].append(lv[100])
        lst["rv_ed_{}".format(comp)][i].append(rv[100])
        lst["lv_es_{}".format(comp)][i].append(lv[300])
        lst["rv_es_{}".format(comp)][i].append(rv[300])



    lst1= [["{0:.2f}".format(s) for s in np.transpose(lst[k])[0]] for k in keylst]
    table = np.transpose([["CASE1", "CASE2", "CASE3"]] + lst1)

    header = ["Patient ID", "LV (ED)", "RV (ED)", "LV (ES)", "RV (ES)"]
    label = "tab:fiber_stress"
    caption = "Average LV and RV fiber stress and end distole and end systole"
    T = tabalize(caption, header, table, label, floatfmt=".3g")
    print(T)


    lst2 = [ np.array(lst1[k], dtype=float) for k in range(4)]
    table=[["{:.2f} \pm {:.2f}".format(np.mean(m), np.std(m)) for m in lst2]]
    header = ["LV (ED)", "RV (ED)", "LV (ES)", "RV (ES)"]
    label = "tab:fiber_stress"
    caption = "Average LV and RV fiber stress and end distole and end systole"
    T = tabalize(caption, header, table, label, floatfmt=".3g")
    print(T)



    
def table3():
    
    es_elastance_lv = {}
    es_elastance_rv = {}
    r = "low"
    for p in patients:

        es_elastance_lv[p] = []
        es_elastance_rv[p] = []

        for a in angles:

            key = "{}:{}:{}".format(p, r, a)

            es_elastance_lv[p].append(results[key]["end_systolic_elastance"])
            es_elastance_rv[p].append(results[key]["end_systolic_elastance_rv"])


    # Table
    lst_lv= [["{0:.2f} $\pm$ {1:.2f}".format(np.mean(es_elastance_lv[p]),
                                           np.std(es_elastance_lv[p]))] \
            for  p in patients]
    lst_rv= [["{0:.2f} $\pm$ {1:.2f}".format(np.mean(es_elastance_rv[p]),
                                           np.std(es_elastance_rv[p]))] \
            for  p in patients]
    case = [["CASE1"], ["CASE2"], ["CASE3"]]
    table = []
    for r in range(3): table.append(case[r] + lst_lv[r] + lst_rv[r])
    header = ["Patient ID", "LV (kPa/ml)", "RV (kPa/ml)"]
    label = "tab:elastance"
    caption = ("Average values of LV and RV end systolic elastance estimated by "+
               "perturbation of model at end-systolic state.")
    T = tabalize(caption, header, table, label, floatfmt=".3g")
    print(T)


     


def table4():

    lst=[]
    keylist = ["forward_times", "nfev", "backward_times", "njev", "run_time"]
    for p in patients:
        for r in ress:
            key = "{}:{}:60".format(p,r)

            d = results_mesh_conv[key]["active_optimization_results"].values()
            n = len(d)
            lst_tmp = [p2key[p],r]
            for k in keylist:

                if k == "run_time":
                 
                    lst_tmp += [np.sum([d[i][k][0] for i in range(len(d))]) / 3600.]
                 
                else:
                    mean = np.mean([np.mean(d[i][k]) for i in range(len(d))])
                    std =  np.std([np.mean(d[i][k]) for i in range(len(d))])
                    lst_tmp += ["{0:.0f} $\pm$ {1:.1f}".format(mean, std)]


            lst.append(lst_tmp)

    header =  ["Patient ID",
               "$\#$ elements",
               r"Forward evalution time (s)",
               r"$\#$ forward evaluations",
                r"Gradient evaluation time (s)",
               r"$\#$ gradient evaluations"]
    

    caption = "Timings for "
    table = lst
    label = "tab:timings"

    T = tabalize(caption, header, table, label, floatfmt=".3g")
    print(T)
    
def tableA1():


    matlst = []
    for p in patients:
        for a in As:

            key = "{}:{}".format(p, a)

            mat = results_startsens[key]["material_parameters"]["a"]
            fval_passive = "{:.2e}".format(results_startsens[key]["passive_optimization_results"]["func_vals"][-1])

            V0lv = results_startsens[key]["simulated_volume"][0]
            V0rv = results_startsens[key]["simulated_volume_rv"][0]
            matlst.append([p2key[p],a] + mat[:1].tolist() + mat[-1:].tolist() + [fval_passive, V0lv, V0rv])


    caption = ("Optimized material paramters in kPA for "+\
               "different initial starting points for $a$.")
    header =  ["Patient ID",
               "Initial $a$",
               r"$a_{\mathrm{LV}}$",
               r"$a_{\mathrm{RV}}$",
               r"$\mathcal{J}_{\mathrm{data}}$ (passive)",
               r"$V_0^{\mathrm{LV}}$",
               r"$V_0^{\mathrm{RV}}$",]
    header = [""]*7
    # lst = [[k] + mat[k].tolist() for k in names]

    table = matlst
    label = "tab:optmal_material_params_start"

    T = tabalize(caption, header, table, label, floatfmt=".3g")
    print(T)
    




if __name__ == "__main__":

    table2()
    table3()
    table4()
    tableA1()

    figure3()
    figure4()
    figure5()
    figure6()
    figure7()
    figureA1()

  
