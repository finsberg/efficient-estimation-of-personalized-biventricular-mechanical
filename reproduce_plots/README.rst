Reproduce figures
=================

This folder contains the script for reproducing the figures in the paper.
In order to reproduce these figures yourselves you need to first install
the requirements

.. code-block:: bash

    pip install -r requirements.txt


Then you simply run

.. code-block:: bash

    python make_figures.py


This will print the tables on the screen and save the figures in a new folder called ``figures``.
