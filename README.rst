Efficient estimation of personalized biventricular mechanical function employing gradient-based optimization
============================================================================================================

This repository contains the source code of the paper `Efficient estimation of personalized 
biventricular mechanical function employing gradient-based optimization <https://doi.org/10.1002/cnm.2982>`_ published in the 
`International Journal for Numerical Methods in Biomedical engineerring` [1]_.

This repository contains the following files and folders

* reproduce_plots
    - This folder contains the code and results used to produce the plots in the paper
* scripts
    - This folder contains the scripts used to run the actual data
* Dockerfile
    - File to build the docker image
* requirements
    - Some python requirements to be installed in the Docker image
* download_data.sh
    - Script for downloading data with FE meshes, strain, pressure and volume.


Running the code in Docker
--------------------------
We have made a prebuillt `Docker <https://www.docker.com>`_ image that you can download in order to test the code yourselves.
This means that you do not have to install anything (except Docker) on you local computer in order to test the code.
To simply run the code, first make sure to install Docker and do

.. code-block::

   docker run -i -t finsberg/efficient-estimation

Alternatively, you can also build your own image by first going to the directory where the ``Dockerfile`` is located, and do

.. code-block::


   docker build -t efficient-estimation .

This will build a docker image on your local machine. Now you can run this by doing

.. code-block::

   docker run -i efficient-estimation


This will bring you into the container where all the necessary software is installed.
Note that if you build your own image, make sure to download the data first.


Running the code on you local machine
-------------------------------------
If you really want to install the software on you local computer please visit the
`Pulse-Adjoint <https://bitbucket.org/finsberg/pulse_adjoint>`_ repository and follow the installation
instructions there. You also need to download the data (see below)



Download the data
------------------
In order to get the finite element meshes, strain, pressure and volume data you need to download this first.
Just do

.. code-block::

   ./download_data.sh


and you will find the data in a new folder called ``data``. 



Citing
------
If you plan to use the code in your own reasearch we would be grateful if you could cite [1]_.



.. [1] Finsberg, H., Xi, C. Tan. JL., Zhong, L., Genet, M.,  Sundnes, J., Lee, LC. and Wall, S.,
       2018. Efficient estimation of personalized biventricular mechanical function employing
       gradient-based optimization. `International journal for numerical methods in biomedical engineering`.

License
-------
2001-2018 Simula Research Laboratory ALL RIGHTS RESERVED
Authors: Henrik Finsberg
END-USER LICENSE AGREEMENT
PLEASE READ THIS DOCUMENT CAREFULLY. By installing or using this software you 
agree with the terms and conditions of this license agreement. If you do not 
accept the terms of this license agreement you may not install or use this software.
Permission to use, copy, modify and distribute any part of this software for 
non-profit educational and research purposes, without fee, and without a written 
agreement is hereby granted, provided that the above copyright notice, and this 
license agreement in its entirety appear in all copies. Those desiring to use this 
software for commercial purposes should contact Simula Research Laboratory AS: post@simula.no
IN NO EVENT SHALL SIMULA RESEARCH LABORATORY BE LIABLE TO ANY PARTY FOR DIRECT, 
INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS, 
ARISING OUT OF THE USE OF THIS SOFTWARE "PULSE-ADJOINT" EVEN IF SIMULA RESEARCH 
LABORATORY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
THE SOFTWARE PROVIDED HEREIN IS ON AN "AS IS" BASIS, AND SIMULA RESEARCH LABORATORY 
HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS. 
SIMULA RESEARCH LABORATORY MAKES NO REPRESENTATIONS AND EXTENDS NO WARRANTIES OF ANY KIND, 
EITHER IMPLIED OR EXPRESSED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
MERCHANTABILITY OR FITNESS

Contributors
------------
* Henrik Finsberg
* Ce Xi
* Ju Le Tan
* Liang Zhong
* Martin Genet
* Joakim Sundnes
* Lik Chuan Lee
* Samuel Wall


Contact
-------
If you have any questions, please do not hesitate to contact Henrik Finsberg at henriknf@simula.no
