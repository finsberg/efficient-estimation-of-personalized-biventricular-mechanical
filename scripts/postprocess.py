import os
import yaml

import numpy as np


import pulse_adjoint.postprocess as post
from pulse_adjoint.setup_optimization import (setup_adjoint_contraction_parameters,
                                              setup_general_parameters)

from params import *




def main():

    angle = 60
    patient = "CASE3"
    resolution = "low"
    active_model = "active_strain"

    


    
    resdir = OUTPATH.format(active_model, patient,
                            resolution, angle)

    
    outdir = os.path.join(filepath, "results")


    key = "{}:{}:{}:{}".format(active_model, patient, resolution, angle)


    params = setup_adjoint_contraction_parameters()

    try:
        with open("{}/input.yml".format(resdir)) as f:
            d = yaml.load(f)
    except:
        print "No input parameters"
        return


    d["Patient_parameters"]["pressure_path"] = pv_path.format(patient)
    d["Patient_parameters"]["mesh_path"] = mesh_path.format(patient,
                                                            resolution,
                                                            angle)
    params.update(d)



    fname = os.path.join(outdir, "all_results.h5")
    geoname = os.path.join(outdir, "all_geo.h5")
    pname = os.path.join(outdir, "all_params.yml")



    res = {}
    setup_general_parameters()
    
    params["sim_file"] = os.path.join(resdir,"result.h5")

    res[key], patient = post.load.get_data(params)
    
    post.load.save_dict_to_h5(res[key], fname, key,
                              overwrite_file=False,
                              overwrite_group=True)
    patient.valve_times = {}
    
    post.load.save_patient_to_h5(patient, geoname, key)
    patient = post.load.load_patient_data(geoname, key)
    
    
    post.load.save_parameters(params, pname, key)
    
    
    res = post.PostProcess(fname,
                           geoname,
                           pname,
                           resdir,
                           recompute = True)
    

        
        
    res.compute("mechanical_features")
    res.make_simulation(refined=False)

    res.compute("volume","strain")
    res.plot_pv_loop()
    res._es = es_points
    res.compute("end_systolic_elastance")

    
    
if __name__ == "__main__":

    main()
