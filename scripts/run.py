#!/usr/bin/env python

from pulse_adjoint.run_full_optimization import main as run
from params import *




def main():

    angle = 60
    patient = "CASE3"
    resolution = "low"
    active_model = "active_strain"


    assert angle in angles
    assert patient in patients
    assert resolution in resolutions
    assert active_model in active_models


    params = get_params()
        
    if active_model == "active_strain":
        params["T_ref"] = 0.4
        
            
    else: 
        params["T_ref"] = 500.0
        params["eta"] = 0.2

            
    params["active_model"] = active_model
    params["Patient_parameters"]["patient"] = patient
    params["Patient_parameters"]["pressure_path"] = pv_path.format(patient)
    params["Patient_parameters"]["mesh_path"] = mesh_path.format(patient,
                                                                 resolution,
                                                                 angle)

       
    outdir = OUTPATH.format(active_model, patient,
                            resolution, angle)

    # Make directory if it does not allready exist
    if not os.path.exists(outdir):
        os.makedirs(outdir)


    # File to store the output
    params["sim_file"] = os.path.join(outdir, "result.h5")

    run(params, False)


if __name__ == "__main__":
    main()
