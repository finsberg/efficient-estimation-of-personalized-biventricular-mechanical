
FROM finsberg/pulse_adjoint
MAINTAINER Henrik Finsberg <henriknf@simula.no>

USER root

# Set working directory to this directory
ENV WORK="$FENICS_HOME/efficient-estimation-of-personalized-biventricular-mechanical"

# Copy the everything here to the container
RUN mkdir $WORK
WORKDIR $WORK
COPY . $WORK/.

# Install python requirements
RUN pip2 install -r requirements.txt
